# Gender Free Names

Gender free names to use in your made-up stories.

# LICENCE

CC-BY

# The List

Ade
Alex
Ali
Andy
Ash
Bern
Bobby
Charlie
Chris
Dan
Dom
Ed
Eli
Fred
Frank
George
Jess
Jo
Jules
Kat
Kim
Kit
Mo
Nat
Nick
Ol
Phil
Ray
Robin
Sam
Steve
Tony
Vic
Viv
Wren
